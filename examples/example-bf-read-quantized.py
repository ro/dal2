#!/usr/bin/env python

# A simple example to
# 1) test if the given *.h5 file has original or quantized data
# 2) accordingly read the first Stokes data for SAP 0 BEAM 0 

import dal
import numpy

def read_hdf5_file(filename):
  f=dal.BF_File(filename)
  sap=f.subArrayPointing(0)
  beam=sap.beam(0)
  quantized=beam.quantized().value[0]
  stokescomp=beam.stokesComponents().value[0]
  print("Stokes %s data quantized: %d"%(stokescomp,quantized))
  if not quantized:
   stokes=beam.stokes(0)
   # storage for data
   xx=numpy.zeros(stokes.dims(),dtype=stokes.dtype) 
   stokes.get2D([0,0],xx)
   print(xx)
  else:
   stokes=beam.qstokes(0)
   scale=stokes.scale()
   offset=stokes.offset()
   datatype=stokes.dataType()
   # check if data are unsigned
   dataUnsigned=datatype=='unsigned char'
   if dataUnsigned:
    data=stokes.dataUInt8()
   else:
    data=stokes.dataInt8()
   # storage for data
   xx=numpy.zeros(data.dims(),dtype=data.dtype)
   xscale=numpy.zeros(scale.dims(),dtype=scale.dtype)
   xoffset=numpy.zeros(offset.dims(),dtype=offset.dtype)
   data.get2D([0,0],xx)
   scale.get2D([0,0],xscale)
   offset.get2D([0,0],xoffset)
   print(xx)
   print(xscale)
   print(xoffset)



if __name__ == '__main__':
  # args thisscript filename.h5
  import sys
  argc=len(sys.argv)
  if argc>1:
   read_hdf5_file(sys.argv[1])
  else:
   print("Usage %s filename.h5"%(sys.argv[0]))

  exit()

