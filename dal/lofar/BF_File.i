%include lofar/BF_File.h

%template(dataInt8) dal::BF_QStokesDataset::data<int8_t>;
%template(dataUInt8) dal::BF_QStokesDataset::data<uint8_t>;
